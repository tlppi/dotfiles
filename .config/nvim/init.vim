nnoremap <space> <Nop>
nnoremap <nowait> <space> :
set nocompatible
if empty(glob('~/.config/nvim/autoload/plug.vim'))
	silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs
				\ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
	autocmd VimEnter * PlugInstall
endif
call plug#begin('~/.config/nvim/plugged')
     Plug 'frazrepo/vim-rainbow'
     Plug 'sheerun/vim-polyglot'
     Plug 'catppuccin/nvim'
     Plug 'itchyny/lightline.vim'
     Plug 'suan/vim-instant-markdown'
     Plug 'powerman/vim-plugin-AnsiEsc'
     Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
     Plug 'https://tildegit.org/sloum/gemini-vim-syntax'
     Plug 'junegunn/fzf.vim'
     Plug 'elkowar/yuck.vim'
     Plug 'cohama/lexima.vim'
     Plug 'SirVer/ultisnips'
     Plug 'honza/vim-snippets'
     Plug 'elzr/vim-json'
     Plug 'tpope/vim-repeat'
     Plug 'tpope/vim-surround'
     Plug 'tpope/vim-vinegar'
     Plug 'junegunn/goyo.vim'
     Plug 'junegunn/limelight.vim'
     Plug 'ap/vim-css-color'
     if has('nvim')
          Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
          Plug 'deoplete-plugins/deoplete-lsp'
     endif
call plug#end()
if (empty($TMUX))
  if (has("nvim"))
    let $NVIM_TUI_ENABLE_TRUE_COLOR=1
  endif
  if (has("termguicolors"))
    set termguicolors
  endif
endif
syntax on
lua << EOF
local catppuccin = require("catppuccin")
-- configure it
catppuccin.setup(
    {
		transparent_background = false,
		term_colors = false,
		styles = {
			comments = "italic",
			functions = "italic",
			keywords = "italic",
			strings = "NONE",
			variables = "NONE",
		},
		integrations = {
			treesitter = true,
			native_lsp = {
				enabled = true,
				virtual_text = {
					errors = "italic",
					hints = "italic",
					warnings = "italic",
					information = "italic",
				},
				underlines = {
					errors = "underline",
					hints = "underline",
					warnings = "underline",
					information = "underline",
				},
			},
			lsp_trouble = false,
			lsp_saga = false,
			gitgutter = false,
			gitsigns = false,
			telescope = false,
			nvimtree = {
				enabled = false,
				show_root = false,
			},
			which_key = false,
			indent_blankline = {
				enabled = false,
				colored_indent_levels = false,
			},
			dashboard = false,
			neogit = false,
			vim_sneak = false,
			fern = false,
			barbar = false,
			bufferline = false,
			markdown = false,
			lightspeed = false,
			ts_rainbow = false,
			hop = false,
		},
	}
)
EOF
let g:rainbow_active = 1
let g:lightline = {
  \ 'colorscheme': 'one',
  \ }
colorscheme catppuccin
filetype indent plugin on "
set clipboard=unnamedplus
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"
let g:UltiSnipsEditSplit="vertical"
let g:depolete#enable_at_startup = 1
let g:deoplete#lsp#use_icons_for_candidates = v:true
set expandtab
set smarttab
set shiftwidth=4
set tabstop=4
set spell
set spelllang=en_gb
set mouse=a
set ttyfast
autocmd BufRead,BufNewFile *.rasi setlocal filetype=css
