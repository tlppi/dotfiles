# edencreeper's zsh config file

autoload -Uz compinit promptinit edit-command-line colors vcs_info
ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE="fg=#ff00ff,bg=magenta,bold,underline"

source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

zstyle ':completion:*' menu select
zstyle ':vcs_info:git:*' formats ' 🐙 %b '
setopt PROMPT_SUBST

zstyle ':vcs_info:*' enable git svn
precmd() {
    vcs_info
}

# VTerm Intergration
vterm_printf(){
    if [ -n "$TMUX" ] && ([ "${TERM%%-*}" = "tmux" ] || [ "${TERM%%-*}" = "screen" ] ); then
        # Tell tmux to pass the escape sequences through
        printf "\ePtmux;\e\e]%s\007\e\\" "$1"
    elif [ "${TERM%%-*}" = "screen" ]; then
        # GNU screen (screen, screen-256color, screen-256color-bce)
        printf "\eP\e]%s\007\e\\" "$1"
    else
        printf "\e]%s\e\\" "$1"
    fi
}

PROMPT='[%F{magenta}%n%F{yellow}@%F{cyan}%M %F{white}%~%B%F{red}${vcs_info_msg_0_}%f%b]%F{green}$ '

setopt HIST_IGNORE_SPACE
setopt HIST_IGNORE_ALL_DUPS
SAVEHIST=10000
HISTSIZE=10000
HISTFILE=~/.zsh_history
autoload -U up-line-or-beginning-search; zle -N up-line-or-beginning-search
autoload -U down-line-or-beginning-search; zle -N down-line-or-beginning-search
setopt autocd
setopt CORRECT
setopt NO_NOMATCH
setopt LIST_PACKED
setopt ALWAYS_TO_END
setopt GLOB_COMPLETE
setopt COMPLETE_ALIASES
setopt COMPLETE_IN_WORD

#export TERM="xterm-256color"
export EDITOR="nvim"
export MANPAGER="sh -c 'col -bx | bat -l man -p'"

 ### PATH
if [ -d "$HOME/.bin" ] ;
  then PATH="$HOME/.bin:$PATH"
fi

if [ -d "$HOME/.local/bin" ] ;
  then PATH="$HOME/.local/bin:$PATH"
fi

if [ -d "$HOME/Applications" ] ;
  then PATH="$HOME/Applications:$PATH"
fi

if [ -d "$HOME/.emacs.d/bin/" ] ;
    then PATH="$HOME/.emacs.d/bin:$PATH"
fi

if [ -d "$HOME/.cargo/bin/" ] ;
    then PATH="$HOME/.cargo/bin:$PATH"
fi


find_current_file(){
    tokens=(${(z)LBUFFER})
    local lastWord="${tokens[-1]}" filepath
    # First assume I'm trying to edit a script. If it's in my path, use it
    filepath="$(which "$lastWord")"
    if [ "$?" -eq 0 ]; then
        tokens[-1]="$filepath"
        LBUFFER="${tokens[@]}"
        return 0
    fi
    # Next try locate with an exact filename match
    filepath="$(locate "*/$lastWord" | sed -n 1p)"
    if [ "$?" -eq 0 ]; then
        tokens[-1]="$filepath"
        LBUFFER="${tokens[@]}"
        return 0
    fi
}
zle -N find_current_file
# ctrl + n
bindkey '^n' find_current_file


function extract {
 if [ -z "$1" ]; then
    # display usage if no parameters given
    echo "Usage: extract <path/file_name>.<zip|rar|bz2|gz|tar|tbz2|tgz|Z|7z|xz|ex|tar.bz2|tar.gz|tar.xz>"
    echo "       extract <path/file_name_1.ext> [path/file_name_2.ext] [path/file_name_3.ext]"
 else
    for n in "$@"
    do
      if [ -f "$n" ] ; then
          case "${n%,}" in
            *.cbt|*.tar.bz2|*.tar.gz|*.tar.xz|*.tbz2|*.tgz|*.txz|*.tar)
                         tar xvf "$n"       ;;
            *.lzma)      unlzma ./"$n"      ;;
            *.bz2)       bunzip2 ./"$n"     ;;
            *.cbr|*.rar)       unrar x -ad ./"$n" ;;
            *.gz)        gunzip ./"$n"      ;;
            *.cbz|*.epub|*.zip)       unzip ./"$n"       ;;
            *.z)         uncompress ./"$n"  ;;
            *.7z|*.arj|*.cab|*.cb7|*.chm|*.deb|*.dmg|*.iso|*.lzh|*.msi|*.pkg|*.rpm|*.udf|*.wim|*.xar)
                         7z x ./"$n"        ;;
            *.xz)        unxz ./"$n"        ;;
            *.exe)       cabextract ./"$n"  ;;
            *.cpio)      cpio -id < ./"$n"  ;;
            *.cba|*.ace)      unace x ./"$n"      ;;
            *)
                         echo "extract: '$n' - unknown archive method"
                         return 1
                         ;;
          esac
      else
          echo "'$n' - file does not exist"
          return 1
      fi
    done
fi
}

# Startx Automatically
if [[ -z $DISPLAY ]] && [[ $(tty) = /dev/tty1 ]]; then exec startx; fi

bindkey "^[[1;5C" forward-word
bindkey "^[[1;5D" backward-word

cdl() # cd and ls after
{
	builtin cd "$@" && exa -al --color=always --group-directories-first
}

	
### ALIASES
PRIV="sudo"

alias grep='grep --color=auto'
alias mirror-update='sudo reflector --verbose --score 100 -l 50 -f 10 --sort rate --save /etc/pacman.d/mirrorlist'

alias vsu="$PRIV nvim"
alias e="emerge"
alias ei="$PRIV emerge --ask"
alias eiq="$PRIV emerge --ask --quiet"
alias es="emerge --search"

alias dmake="$PRIV make clean install && make clean"
alias dclean="make clean && git reset --hard origin/master"

alias ls='exa -al --color=always --group-directories-first' # my preferred listing
alias la='exa -a --color=always --group-directories-first'  # all files and dirs
alias ll='exa -l --color=always --group-directories-first'  # long format
alias lt='exa -aT --color=always --group-directories-first' # tree listing
alias l.='exa -a | egrep "^\."'

alias c='cd ~/.config'
alias b='cd ~/.local/bin'
alias d='cd ~/.local/src/dwm'

alias gb='git branch'
alias gm='git merge'
alias gc='git commit'
alias go='git checkout'
alias ga='git add'
alias gs='git status'
alias gp='git apply'

alias x='chmod +x'

alias pp="curl -F 'f:1=<-' ix.io"
alias p="curl -F '_=<-' https://p.teotech.xyz"

alias neofetch='neofetch --kitty $HOME/Pictures/neofetch.png'

alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'

eval "$(starship init zsh)"
