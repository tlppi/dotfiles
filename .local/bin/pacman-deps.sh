#!/usr/bin/env bash

if [ -z "$1" ];then
	echo "need filename"
	exit 0
fi
sudo pacman -S --asdeps --needed $(pacman -Si $1 | sed -n '/^Opt/,/^Conf/p' | sed '$d' | sed 's/^Opt.*://g' | sed 's/^\s*//g' | tr '\n' ' ')
