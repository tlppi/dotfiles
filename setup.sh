#!/usr/bin/env bash

okprefix='[\033[;32m*\033[0m]'
errprefix='[\033[0;31m*\033[0m]'
warnprefix='[\033[0;33m*\033[0m]'

if (($EUID == 0)); then
    echo -e "${errprefix} Please do not run as root. Exiting"
    exit
fi

echo -e "${okprefix} -- eden's Install script --"
echo -e "${warnprefix} This script is a work in progress. Use at your own risk"
echo -e ""
echo -e "${okprefix} This script will install packages and dotfiles"
echo -e "${okprefix} It will create backups of any config files"

echo -e -n "${okprefix} "
read -r -p "Are you sure you want to continue? [y/N] " input

case $input in
    [yY][eE][sS]|[yY])
	echo -e "${okprefix} Great!"
	;;
    [nN][oO]|[nN])
	echo -e "${errprefix} Sure. Exiting"
	exit
	;;
    *)
	echo -e "${errprefix} Sure. Exiting"
	exit
	;;
esac

echo ""
echo ""

if command -v pacman &> /dev/null; then
    echo -e "${okprefix} Pacman is installed. Continueing"
else
    # [TODO] Ensure that this works. Haven't had a chance to run script on a non-pacman system.

    echo -e "${warrnprefix} Could not find pacman installed on system."
    echo -e "${warrnprefix} Assuming this is not an arch system. Will not install dependencies"

    if command -v git &> /dev/null; then
	    echo -e "${errprefix} Git is not installed."
	    echo -e "${errprefix} Please install git and make sure it is in your PATH."
	    echo -e "${errprefix} Exiting"
	    exit 1
    fi
fi

echo -e "${okprefix} Updating system"
sudo pacman -Syu --noconfirm


if command -v paru &>/dev/null; then
    echo -e "${okprefix} Paru detected. Installing dependencies"
    paru -S --needed --noconfirm python-pip bspwm sxhkd rofi polybar picom-ibhagwan-git nitrogen xorg dunst eww spotify playerctl sysstat kitty zsh zsh-autosuggestions zsh-syntax-highlighting ffmpeg imagemagick i3lock-color qutebrowser nm-connection-editor git exa instant-markdown-d lxsession
elif command -v yay &>/dev/null; then
    echo -e "${okprefix} Yay detected. Installing dependencies"
    yay -S --needed --noconfirm python3-pip bspwm sxhkd rofi polybar picom-ibhagwan-git nitrogen xorg dunst eww spotify playerctl sysstat kitty zsh zsh-autosuggestions zsh-syntax-highlighting ffmpeg imagemagick i3lock-color qutebrowser nm-connection-editor git exa instant-markdown-d lxsession
else
    echo -e "${warnprefix} No AUR helper detected."
    echo -e -n "${warnprefix} "
    read -r -p "Would you like to install paru? [y/N]: " paru

    case $paru in
      [yY])
	echo -e "${okprefix} Installing git and base-devel before other dependencies"
	sudo pacman -S git base-devel --needed --noconfirm
	echo -e "${okprefix} Installing paru"
	rm /tmp/paru-bin -rf
	git clone https://aur.archlinux.org/paru-bin /tmp/paru-bin
	cd /tmp/paru-bin && makepkg -si
	echo -e "${okprefix} Paru installed. Installing dependencies"
	paru -S --needed --noconfirm python3-pip bspwm sxhkd rofi polybar picom-ibhagwan-git nitrogen xorg dunst eww spotify playerctl sysstat kitty zsh zsh-autosuggestions zsh-syntax-highlighting ffmpeg imagemagick i3lock-color qutebrowser nm-connection-editor git exa instant-markdown-d lxsession
	;;
      [nN])
	echo -e "${okprefix} Okay. Will not install dependencies" ;;
      *)
	echo -e "${okprefix} Okay, Will not install dependencies" ;;
    esac
fi

echo -e "${okprefix} Installing dotfiles"

if [[ -d "$HOME/.cfg" ]]; then
    echo "${okprefix} ~/.cfg exists. Moving to ~/.cfg.bak"
    mv ~/.cfg ~/.cfg.bak
fi

echo -e "${okprefix} Cloning repo"
cd
function config {
    /usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME $@
}

echo ".cfg" >> ~/.gitignore
git clone --bare https://git.teotech.xyz/teo/dotfiles $HOME/.cfg


config checkout
if [ $? = 0 ]; then
    config config status.showUntrackedFiles no
    echo -e "${okprefix} Checked out!"
else
    echo ${okprefix} Backing up existing dotfiles to ~/.cfg-backup
    mkdir ~/.config-backup
    config checkout 2>&1 | egrep "\s+\." | awk {'print $1'} | xargs -I{} mv {} .cfg-backup/{}
    echo -e "${okprefix} Any existing dotfiles are backed up to ~/.cfg-backup"
    echo -e "${okprefix} Checking out dotfiles"
    config checkout
    config config status.showUntrackedFiles no
    echo -e "${okprefix} Checked out!"
fi

echo -e "${okprefix} Doing various other final configuraton steps"

if command -v pip &> /dev/null; then
    pip install neovim
else
    echo "${warnprefix} pip was not found on the system. Neovim will not work. Install pip and run `pip install neovim`"
fi

chsh -s `which zsh`
