-- --- edencreeper's XMonad config ---

-- Imports
import XMonad
import Data.Monoid
import System.Exit
import XMonad.Util.EZConfig (additionalKeysP)
import qualified XMonad.StackSet as W
import qualified Data.Map        as M
import qualified XMonad.StackSet as W
import qualified XMonad.Actions.Search as S
import qualified XMonad.Layout.ToggleLayouts as T (toggleLayouts, ToggleLayout(Toggle))
import XMonad.Hooks.ManageDocks (avoidStruts, docksEventHook, manageDocks, ToggleStruts(..))
import XMonad.Hooks.EwmhDesktops
import XMonad.Layout.MultiToggle (mkToggle, single, EOT(EOT), (??))
import qualified XMonad.Layout.MultiToggle as MT (Toggle(..))
import XMonad.Actions.CopyWindow (kill1)
import XMonad.Actions.WithAll (sinkAll, killAll)
import System.Exit (exitSuccess)
import XMonad.Actions.CycleWS (Direction1D(..), moveTo, shiftTo, WSType(..), nextScreen, prevScreen)
import XMonad.Hooks.DynamicLog (dynamicLogWithPP, wrap, xmobarPP, xmobarColor, shorten, PP(..))
import XMonad.Util.Run (runProcessWithInput, safeSpawn, spawnPipe, hPutStrLn)
import XMonad.Util.SpawnOnce
import XMonad.Layout.Simplest
import XMonad.Util.NamedScratchpad
import XMonad.Actions.Promote
import XMonad.Actions.RotSlaves (rotSlavesDown, rotAllDown)
import XMonad.Layout.NoBorders
import XMonad.Layout.LimitWindows (limitWindows, increaseLimit, decreaseLimit)
import XMonad.Layout.MultiToggle.Instances (StdTransformers(NBFULL, MIRROR, NOBORDERS))
import XMonad.Layout.ResizableTile
import XMonad.Actions.WindowGo (runOrRaise)
import Data.Maybe (isJust, fromJust)
import XMonad.Actions.MouseResize
import XMonad.Layout.WindowArranger (windowArrange, WindowArrangerMsg(..))
import Data.Char (isSpace, toUpper)
import Data.Monoid
import XMonad.Layout.Renamed
import XMonad.Layout.SubLayouts
import XMonad.Layout.LayoutModifier
import XMonad.Layout.Spacing
import XMonad.Layout.SimplestFloat
import XMonad.Layout.GridVariants (Grid(Grid))
import XMonad.Layout.Spiral


-- Set our stuff... and things
myTerminal :: String
myTerminal = "kitty"  -- Our terminal emulator

myBrowser :: String
myBrowser = "qutebrowser" -- Our browser

myBorderWidth :: Dimension
myBorderWidth = 4 -- How wide our window borders are

myModMask :: KeyMask
myModMask = mod4Mask -- Set our modkey to be the "Windows" key

myFocusFollowsMouse :: Bool
myFocusFollowsMouse = True -- Whether the focused window follows the mouse

myClickJustFocuses :: Bool
myClickJustFocuses = False -- Whether clicking to focus a window also sends that to the window

mySpacing :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing i = spacingRaw False (Border i i i i) True (Border i i i i) True
mySpacing' :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing' i = spacingRaw True (Border i i i i) True (Border i i i i) True

myWorkspaces    = ["1","2","3","4","5","6","7","8","9"]
myWorkspaceIndices = M.fromList $ zipWith (,) myWorkspaces [1..] -- (,) == \x y -> (x,y)
clickable ws = "<action=xdotool key super+"++show i++">"++ws++"</action>"
    where i = fromJust $ M.lookup ws myWorkspaceIndices

myNormColor  = "#ff79c6"
myFocusColor = "#8be9fd"


-- Keybindings
myKeys :: [(String, X ())]
myKeys =
  [
  -- XMonad
    ("M-C-r", spawn "xmonad --recompile") -- Restart xmonad
  , ("M-S-r", spawn "xmonad --restart") -- Recompile xmonad
  , ("M-S-q", io exitSuccess) -- Quit xmonad

  -- DMENU
  , ("M-S-<Return>", spawn "dmenu_run -i -p \"Run: \"") -- App Launcher
  , ("M-p c", spawn "dm-colpick") -- Colour Picker
  , ("M-p e", spawn "dm-conf") -- Config Editor
  , ("M-p x", spawn "dm-kill") -- Process Killer
  , ("M-p s", spawn "dm-maim") -- Screenshot Taker
  , ("M-p l", spawn "dm-logout") -- Power Menu
  , ("M-p a", spawn "dm-man") -- Man Pagerer
  , ("M-p p", spawn "passmenu") -- Password Menu
  , ("M-p m", spawn "music-controller") -- Music Controller

  -- Emacs
  , ("M-e e", spawn "emacsclient -c -a 'emacs'")
  , ("M-e b", spawn "emacsclient -c -a 'emacs' --eval '(ibuffer)'")
  , ("M-e d", spawn "emacsclient -c -a 'emacs' --eval '(dired nil)'")
  , ("M-e <Return>", spawn "emacsclient -c -a 'emacs' --eval '(+verm/here nil)'")

  -- Useful Stuff to have on keybinds
  , ("M-<Return>", spawn (myTerminal))
  , ("M-w", spawn (myBrowser))
  , ("M-s", spawn "spotify")
  , ("M-d", spawn "discord")
  , ("M-M1-h", spawn (myTerminal ++ "-e htop"))
  , ("M-i", spawn "popinfo-notif")
  , ("M-S-s", spawn "maim -s | xclip -selection clipboard -t image/png")


  -- Kill Windows
  , ("M-S-c", kill1) -- Kill the currently focused client
  , ("M-S-a", killAll) -- Kill all windows on current workspace

  -- Workspaces
  , ("M-.", nextScreen)  -- Switch focus to next monitor
  , ("M-,", prevScreen)  -- Switch focus to prev monitor
  , ("M-S-<KP_Add>", shiftTo Next nonNSP >> moveTo Next nonNSP)       -- Shifts focused window to next ws
  , ("M-S-<KP_Subtract>", shiftTo Prev nonNSP >> moveTo Prev nonNSP)  -- Shifts focused window to prev ws

  -- Floating Windows
  , ("M-f", sendMessage (T.Toggle "floats")) -- Toggle Floating layout
  , ("M-t", withFocused $ windows . W.sink) -- Push floating window into tile
  , ("M-S-t", sinkAll) -- Push all floating windows into tiling

  -- Windows navigation
  , ("M-m", windows W.focusMaster)  -- Move focus to the master window
  , ("M-j", windows W.focusDown)    -- Move focus to the next window
  , ("M-k", windows W.focusUp)      -- Move focus to the prev window
  , ("M-S-m", windows W.swapMaster) -- Swap the focused window and the master window
  , ("M-S-j", windows W.swapDown)   -- Swap focused window with next window
  , ("M-S-k", windows W.swapUp)     -- Swap focused window with prev window
  , ("M-<Backspace>", promote)      -- Moves focused window to master, others maintain order
  , ("M-S-<Tab>", rotSlavesDown)    -- Rotate all windows except master and keep focus in place
  , ("M-C-<Tab>", rotAllDown)       -- Rotate all the windows in the current stack

   -- Layouts
  , ("M-<Tab>", sendMessage NextLayout)           -- Switch to next layout
  , ("M-<Space>", sendMessage (MT.Toggle NBFULL) >> sendMessage ToggleStruts) -- Toggles noborder/full
 
  -- Window resizing
  , ("M-h", sendMessage Shrink)                   -- Shrink horiz window width
  , ("M-l", sendMessage Expand)                   -- Expand horiz window width
  , ("M-M1-j", sendMessage MirrorShrink)          -- Shrink vert window width
  , ("M-M1-k", sendMessage MirrorExpand)          -- Expand vert window width

  -- Multimedia Keys
  --, ("<XF86AudioPlay>", spawn (myTerminal ++ "mocp --play"))
  --, ("<XF86AudioPrev>", spawn (myTerminal ++ "mocp --previous"))
  --, ("<XF86AudioNext>", spawn (myTerminal ++ "mocp --next"))
  --, ("<XF86AudioMute>", spawn "amixer set Master toggle")
  --, ("<XF86AudioLowerVolume>", spawn "amixer set Master 5%- unmute")
  --, ("<XF86AudioRaiseVolume>", spawn "amixer set Master 5%+ unmute")
  --, ("<XF86HomePage>", spawn "qutebrowser https://www.youtube.com/c/DistroTube")
  --, ("<XF86Search>", spawn "dmsearch")
  --, ("<XF86Mail>", runOrRaise "thunderbird" (resource =? "thunderbird"))
  --, ("<XF86Calculator>", runOrRaise "qalculate-gtk" (resource =? "qalculate-gtk"))
  --, ("<XF86Eject>", spawn "toggleeject")
  --, ("<Print>", spawn "dmscrot")
  ]

  where nonNSP          = WSIs (return (\ws -> W.tag ws /= "NSP"))
        nonEmptyNonNSP  = WSIs (return (\ws -> isJust (W.stack ws) && W.tag ws /= "NSP"))



-- Mouse Bindings
myMouseBindings (XConfig {XMonad.modMask = modm}) = M.fromList $
    [ ((modm, button1), (\w -> focus w >> mouseMoveWindow w
                                       >> windows W.shiftMaster))
    , ((modm, button2), (\w -> focus w >> windows W.shiftMaster))
    , ((modm, button3), (\w -> focus w >> mouseResizeWindow w
                                       >> windows W.shiftMaster))
    ]

-- Layouts
myLayoutHook = avoidStruts $ mouseResize $ windowArrange $ T.toggleLayouts floats
               $ mkToggle (NBFULL ?? NOBORDERS ?? EOT) myDefaultLayout
             where
               myDefaultLayout =     withBorder myBorderWidth tall
                                 ||| floats
                                 ||| grid
                                 ||| spirals
-- Layouts
tall     = renamed [Replace "tall"]
           $ smartBorders
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 12
           $ mySpacing 8
           $ ResizableTall 1 (3/100) (1/2) []
floats   = renamed [Replace "floats"]
           $ smartBorders
           $ limitWindows 20 simplestFloat
grid     = renamed [Replace "grid"]
           $ smartBorders
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 12
           $ mySpacing 8
           $ mkToggle (single MIRROR)
           $ Grid (16/10)
spirals  = renamed [Replace "spirals"]
           $ smartBorders
           $ subLayout [] (smartBorders Simplest)
           $ mySpacing' 8
           $ spiral (6/7)

myLayout = tiled ||| Mirror tiled ||| Full
  where
     -- default tiling algorithm partitions the screen into two panes
     tiled   = Tall nmaster delta ratio

     -- The default number of windows in the master pane
     nmaster = 1

     -- Default proportion of screen occupied by master pane
     ratio   = 1/2

     -- Percent of screen to increment by when resizing panes
     delta   = 3/100

-- Window rules:

myManageHook = composeAll
    [ className =? "MPlayer"        --> doFloat
    , className =? "Gimp"           --> doFloat
    , resource  =? "desktop_window" --> doIgnore
    , resource  =? "kdesktop"       --> doIgnore ]


------------------------------------------------------------------------
-- Status bars and logging

-- Perform an arbitrary action on each internal state change or X event.
-- See the 'XMonad.Hooks.DynamicLog' extension for examples.
--
myLogHook :: X ()
myLogHook = return ()

-- What to start upon launch
myStartupHook = do
  spawnOnce "nitrogen --restore &"
  spawnOnce "picom &"
  spawnOnce "lxsession &"
  spawnOnce "xrdb -merge $HOME/.Xresources"
  spawnOnce "setxkbmap gb &"
  spawnOnce "trayer --edge top --align right --widthtype request --padding 6 --SetDockType true --SetPartialStrut true --expand true --transparent true --alpha 0 --tint 0x292d3e --height 20 &"
  spawnOnce "xsetroot -cursor_name left_ptr"
  spawnOnce "dunst &"
  spawnOnce "emacs --daemon &"


-- THE XMONAD!!
main :: IO ()
main = do
  xmproc <- spawnPipe "xmobar -x 0 ~/.config/xmobar/xmobarrc"
  xmonad $ ewmh def
    { manageHook         = myManageHook <+> manageDocks
    , handleEventHook    = docksEventHook <+> fullscreenEventHook
    , modMask            = myModMask
    , terminal           = myTerminal
    , startupHook        = myStartupHook
    , layoutHook         = myLayoutHook
    , workspaces         = myWorkspaces
    , borderWidth        = myBorderWidth
    , normalBorderColor  = myNormColor
    , focusedBorderColor = myFocusColor
    , logHook = dynamicLogWithPP $ xmobarPP {
                ppOutput = hPutStrLn xmproc
                , ppTitle = xmobarColor "#FF79C6" "" . shorten 50
                , ppCurrent = xmobarColor "#BD93F9" "" . wrap "[" "]"
                , ppVisible = xmobarColor "#BD93F8" "" . clickable
                , ppHidden = xmobarColor "#FF79C6" "" . wrap " " " " . clickable
                , ppUrgent = xmobarColor "#FF5555" "" . wrap "!" "!"
                , ppHiddenNoWindows = xmobarColor "#4D4D4D" "" . wrap " " " " . clickable
                , ppSep = " | "
      }
    } `additionalKeysP` myKeys
